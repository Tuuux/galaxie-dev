# galaxie-dev

Create your local Gitea (source code repository, similar to GitHub), and a Drone.io instance via docker-compose. 

The Portainer Management UI is also included in the design.
# Install Docker
## Uninstall old versions
Older versions of Docker were called ``docker``, ``docker.io``, or ``docker-engine``. If these are installed, uninstall them:
```shell script
sudo apt-get remove docker docker-engine docker.io containerd runc
```
## Debian Install
### Dependencies
* Update the apt package index and install packages to allow apt to use a repository over HTTPS.
```shell script
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
* Add Docker’s official GPG key
```shell script
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
* Use the following command to set up the stable repository
```shell script
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
```
### Install Docker engine
```shell script
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
### Summary
```shell script
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update && \
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common && \
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - && \
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable" && \
sudo apt-get update && \
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### User permission
If you would like to use Docker as a non-root user, you should now consider adding your user to the “docker” group with something like:
```shell script
sudo usermod -aG docker your-user
```
Here you have to reload you session (relog or reboot)

# Portainer 
http://localhost:9000
  
Use the following Docker commands to deploy the Portainer Server; note the agent is not needed on standalone hosts, however it does provide additional functionality if used (see portainer and agent scenario below):

```shell script
cd docker
docker volume create portainer_data || True
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```

**Note:** Port 9000 is the general port used by Portainer for the UI access. Port 8000 is used exclusively by the EDGE agent for the reverse tunnel function. If you do not plan to use the edge agent, you do not need to expose port 8000

```shell script
cd docker
docker volume create portainer_data || True
docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```

# Gitea
  http://localhost:8380
  
  Postgres Installation: [Use gitea-db:5432 as host](https://raw.githubusercontent.com/srcmkr/gitea-drone-docker-compose/master/gitea-setup.JPG)
  
  Hint: If contribution graph doesn't show up, just `docker-compose restart`
  
# Drone 
  http://localhost:8381 (don't forget to use the sync button)

