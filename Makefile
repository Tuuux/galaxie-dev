.PHONY: all prepare-dev venv lint test run shell clean build install docker
SHELL=/bin/bash

#!make
include .env
export $(shell sed 's/=.*//' .env)
# export


#PYTHON=${VENV_BIN}/python3

all:
	@echo "make prepare-dev"
	@echo "    Install system requirements"
	@echo "make env"
	@echo "    Create python virtual environment and install dependencies."
	@echo "make test"
	@echo "    Run tests on project."
	@echo "make clean"
	@echo "    Remove python artifacts and virtualenv."

install-docker:
	apt-get remove docker docker-engine docker.io containerd runc
	apt-get update
	apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
	curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
	add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(shell lsb_release -cs) stable"
	apt-get update
	apt-get install docker-ce docker-ce-cli containerd.io docker-compose
	test -f /etc/bash_completion.d/docker-compose || wget -N -nv -P /etc/bash_completion.d/ https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose

install-python:
	apt install -y python3 python3-pip python3-venv

venv-create:
	@ test -d $(GLXDEV_VENV_DIRECTORY) || cd $(GLXDEV_DIRECTORY) && python3 -m venv $(GLXDEV_VENV_NAME)

requirements: venv-create
	@ ${GLXDEV_VENV_ACTIVATE} &&\
	which ansible-playbook && ansible-playbook  playbooks/tasks/pip_requirements.yml ||	pip install -U -q pip setuptools wheel; pip install -U -q pip -r $(GLXDEV_PYTHON_REQUIREMENTS_FILE)

prepare-all: prepare-alpine prepare-openwrt

prepare-directories: requirements
	@ ${GLXDEV_VENV_ACTIVATE} && ansible-playbook  playbooks/tasks/prepare_directories.yml

prepare-openwrt: prepare-directories
	@ ${GLXDEV_VENV_ACTIVATE} && ansible-playbook  playbooks/tasks/prepare_openwrt.yml

prepare-alpine: prepare-directories
	@ ${GLXDEV_VENV_ACTIVATE} && ansible-playbook  playbooks/tasks/prepare_alpine.yml

download-all: download-alpine download-openwrt

download-alpine: prepare-alpine
	@ ${GLXDEV_VENV_ACTIVATE} && ansible-playbook  playbooks/tasks/download_alpine.yml

download-openwrt: prepare-openwrt
	@ ${GLXDEV_VENV_ACTIVATE} && ansible-playbook  playbooks/tasks/download_openwrt.yml

roles: requirements
	@ ${GLXDEV_VENV_ACTIVATE} && ansible-galaxy install -f -p roles/ -r galaxy.yml

#test: requirements
#	@ ${VENV_ACTIVATE} && cd ${TEST_DEV_DIRECTORY} && ansible-playbook main.yml

test:
	env
clean:
	rm -rf ./venv
	rm -rf ~/.cache/pip
	docker image prune -a -f

destroy-everything:
	rm -rf ./venv
	rm -rf ~/.cache/pip
	docker/docker-clean.sh

docker-network:
	docker network create galaxie-dev || true

portainer:: docker-network
	COMPOSE_FILE=stacks/portainer/docker-compose.yml docker-compose up --no-deps --build
	# docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce

docker: docker-network
	COMPOSE_FILE=docker/docker-compose.yml docker-compose up --no-deps --build

compose-gitea:: docker-network
	@ ${VENV_ACTIVATE} && which ansible-playbook ./playbooks/dev_gitea.yml && COMPOSE_FILE=docker/stacks/gitea/docker-compose.yml docker-compose up --no-deps --build || false

#download-rootfs: requirements
#	@ ${VENV_ACTIVATE} && ansible-playbook  playbooks/download_rootfs.yml

build-debian: download-rootfs
	docker build --tag galaxie-debian:stable $(ROLES_FILES_DIRECTORY)/docker/Debian/stable/.

build-openwrt: download-rootfs
	docker build --tag galaxie-openwrt:snapshots $(ROLES_FILES_DIRECTORY)/docker/OpenWrt/snapshots/.